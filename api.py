import os
from eve import Eve
from eve_swagger import get_swagger_blueprint, add_documentation
from flasgger import Swagger
from flask_cors import CORS

from settings import EVE_SETTINGS, filter_provenance_fetched, on_fetched_resource
from custom import endpoints, monitor
import descriptions


"""
Create Eve app
"""
app = Eve(settings=EVE_SETTINGS)
app.on_fetched_resource_provenance += filter_provenance_fetched
app.on_fetched_resource += on_fetched_resource
CORS(app)


"""
Configure eve_swagger
This adds an /api-docs endpoint, that returns the swagger documentation in json
"""
eveswagger = get_swagger_blueprint()

app.register_blueprint(eveswagger)

app.config['SWAGGER_INFO'] = {
    'title': 'FlowMaps Covid-19 API',
    'version': '1.0',
    'description': descriptions.main_description,
    # 'termsOfService': 'terms of service',
    # 'contact': {
    #     'name': 'FlowMaps team at BSC',
    #     'url': 'https://flowmaps.life.bsc.es/flowboard/about'
    # },
    # 'license': {
    #     'name': 'BSD',
    #     'url': 'https://github.com/pyeve/eve-swagger/blob/master/LICENSE',
    # },
    'schemes': ['http', 'https'],
}

# app.config['SWAGGER_HOST'] = 'https://myhost.com'


"""
Configure flasgger
This adds the flassger UI at /apidocs
The flassger UI renders the contents of /api-docs json
"""
config = Swagger.DEFAULT_CONFIG
config['specs'][0]['endpoint'] = 'api-docs'
config['specs'][0]['route'] = '/api-docs'
Swagger(app, config=config)


"""
Add custom endpoints
"""
mongodb_params = {
    "host": [EVE_SETTINGS['MONGO_HOST']],
    "username": EVE_SETTINGS['MONGO_USERNAME'],
    "password": EVE_SETTINGS['MONGO_PASSWORD'],
    "authSource": EVE_SETTINGS['MONGO_AUTH_SOURCE'],
    "readPreference": EVE_SETTINGS.get('MONGO_READ_PREFERENCE', "primaryPreferred"),
    "serverSelectionTimeoutMS": 5000,
    "socketTimeoutMS": 5000,
    "connectTimeoutMS": 5000,
}
db = endpoints.init_mongodb(mongodb_params)
check_collections = ['layers', 'layers.data', 'layers.data.consolidated', 'mitma_mov.daily_mobility_matrix']

@app.route('/ping')
def ping():
    try:
        assert all(c in db.list_collection_names() for c in check_collections)
        return {'status': 'ok'}
    except Exception as e:
        return {'status': 'ko', 'error': f'{type(e).__name__}: {e}'}

@app.route('/risk_dates')
def risk_dates():
    return endpoints.get_risk_dates(db)

@app.route('/incidence_dates')
def incidence_dates():
    return endpoints.get_incidence_dates(db)

@app.route('/outgoing_risk')
def outgoing_risk():
    return endpoints.get_outgoing_risk(db)

@app.route('/outgoing_risk_multiple_sources')
def outgoing_risk_multiple_sources():
    return endpoints.get_outgoing_risk_multiple_sources(db)

@app.route('/incoming_risk')
def incoming_risk():
    return endpoints.get_incoming_risk(db)

@app.route('/incoming_risk_multiple_targets')
def incoming_risk_multiple_targets():
    return endpoints.get_incoming_risk_multiple_targets(db)

@app.route('/incoming_risk_history')
def incoming_risk_history():
    return endpoints.get_incoming_risk_history(db)

@app.route('/outgoing_risk_history')
def outgoing_risk_history():
    return endpoints.get_outgoing_risk_history(db)

@app.route('/aggregated_incoming_risk')
def aggregated_incoming_risk():
    return endpoints.get_aggregated_incoming_risk(db)

@app.route('/aggregated_outgoing_risk')
def aggregated_outgoing_risk():
    return endpoints.get_aggregated_outgoing_risk(db)

@app.route('/incidence')
def incidence():
    return endpoints.get_incidence(db)

@app.route('/aggregate')
def aggregate():
    return endpoints.get_aggregate(db)

@app.route('/monitor')
def monitor_all():
    return monitor.monitor_all(db)

@app.route('/total_incoming_daily_mobility')
def total_incoming_daily_mobility():
    return endpoints.get_total_incoming_daily_mobility(db)

@app.route('/total_outgoing_daily_mobility')
def total_outgoing_daily_mobility():
    return endpoints.get_total_outgoing_daily_mobility(db)

@app.route('/distinct')
def distinct():
    return endpoints.distinct(db)


if __name__ == '__main__':
    app.run(debug=True, port=8081)
