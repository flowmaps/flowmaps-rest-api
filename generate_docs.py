import descriptions
from grip import export

if __name__ == '__main__':
    with open('DOCS.md', 'w') as f:
        f.write(descriptions.main_description)
        f.write(descriptions.layers)
        f.write(descriptions.layers_data)
        f.write(descriptions.layers_data_consolidated)
        f.write(descriptions.mitma_mov_movements_raw)
        f.write(descriptions.mitma_mov_zone_movements)
        f.write(descriptions.daily_mobility_matrix)
        f.write(descriptions.provenance)

    export(path='DOCS.md')
