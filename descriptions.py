empty = ''


main_description = '''

<br>
<a href="https://www.bsc.es/"><img src="https://flowmaps.life.bsc.es/flowboard/static/img/BSC-blue.svg" height="100"></img></a>
<br>
<br>
<br>

This API provides access to the **FlowMaps** database which combines Spanish mobility data, COVID-19 and other related data. 

The database integrates two main types of data:

1. Time dependent population **mobility networks** across Spain (provided by MITMA and INE)

2. Daily reports of **COVID-19** cases in Spain, at different levels of spatial resolution (provided by the CNE and the different Autonomous Communities)

All the data records are associated with a specific area from a geographic layer:

3. **Geographic layers** for Spain, in geojson format, at different levels of spatial resolution.


All the data has been gather from official access points. Look <a href="https://flowmaps.life.bsc.es/flowboard/data">here</a> and at the `/provenance` collection for more info about origin of the data.


This API is used by the flowmaps website: <a href="https://flowmaps.life.bsc.es/flowboard">https://flowmaps.life.bsc.es/flowboard</a>

<a href="https://flowmaps.life.bsc.es/flowboard/contact">Contact us</a>

'''


layers = '''
## /layers

Geojson layers. This endpoint can be used to download multiple geographic layers in <a href="https://en.wikipedia.org/wiki/GeoJSON">geojson</a> format.<br>

Data contained in the rest of collections is associated with one of this layers using a **layer** identifier.

Each entry in layers has geographical coordinates valid over ETRS89 (epsg:4258) spheroid. These entries have next structure:

    {
        id: The unique id of the geographical feature within the layer
        layer: The name of the layer
        feat: A GeoJSON ‘Feature’ object. ‘feat.geometry’ has a 2dsphere index
        centroid: The centroid of the geographical feature in classical [lon,lat] format. It is indexed
    }


Available layers (with its identifiers): 

* ine_mov
    - ine_mov : Áreas de movilidad INE

* mitma_mov
    - mitma_mov : Áreas de movilidad MITMA

* mitma_mun_mov
    - mitma_mun_mov : Áreas de movilidad con municipios MITMA

* ine_sec
    - ine_sec : Secciones censales INE

* cnig_pob
    - cnig_municipios : Municipios CNIG
    - cnig_provincias : Provincias CNIG
    - cnig_ccaa : Comunidades Autónomas CNIG

* world
    - world : World map

* ate_pri
    - ate_pri : Centros de atención primaria

* ate_urg_extra
    - ate_urg_extra : Centros de atención urgente extrahospitalaria

* hospitales
    - hospitales : Centros de atención urgente extrahospitalaria

* 09
    - reg_san_09 : Regions Sanitàries GenCat
    - sect_san_09 : Sectors Sanitaris GenCat
    - aga_09 : Àrees de Gestió Assitencial GenCat
    - abs_09 : Àrees Bàsiques de Salut GenCat
    - shires_09 : Comarques Catalunya

* 13
    - mun_dis_13 : Municipio+distrito de Madrid
    - zon_bas_13 : Zonas básicas sanitarias de Madrid
    - area_san_13 : Áreas de Salud C. Madrid
    - dis_san_13 : Distritos de Salud C. Madrid
    - zon_san_13 : Zonas de Salud C. Madrid
    - zbs_13 : Zonas Básicas de Salud C. Madrid

* 16
    - oe_16 : Osasun Eremuak (Zonas de Salud) Euskadi
    - cs_hosp_16 : Centros de salud, ambulatorios y hospitales públicos de Euskadi

* 15
    - zbs_15 : Zonas Básicas de Salud del Servicio Navarro de Salud
    - as_15 : Areas de Salud del Servicio Navarro de Salud

* 07
    - zbs_07 : Zonas Básicas de Salud de CyL

* codigos_postales
    - codigos_postales : Códigos Postales

* 12 (unused)
    - comarcas_12 : Comarcas de Galicia
    - concellos_12 : Concellos de Galicia
    - parroquias_12 : Comarcas de Galicia

* 01 (unused)
    - dis_san_01 : Distritos sanitarios de Andalucía
    - zbs_01 : Zonas básicas de Salud de Andalucía
    - d_zbs_01 : Demarcaciones dentro de Zonas básicas de Salud de Andalucía

* andorra (unused)
    - andorra : Andorra


Example for retrieving one layer:

    https://flowmaps.life.bsc.es/api/layers?where={"layer":"cnig_provincias"}

Example document:

    {
        "_id": "cnig_provincias:35",
        "id": "35",
        "layer": "cnig_provincias",
        "feat": {
            "type": "Feature",
            "geometry": {
                "type": "MultiPolygon",
                "coordinates": [...]
            },
            "properties": {
                "fecha_alta": "2005/12/31",
                "fecha_baja": null,
                "rotulo": "Las Palmas",
                "id_ccaa": "65",
                "id_prov": "35",
                "g1_s1_txt": "Las Palmas",
                "g2_s1_txt": null,
                "g1_orden": 1,
                "g2_orden": 0,
                "g1_lengua": "spa",
                "g2_lengua": null,
                "x_cvi": -14.79451709,
                "y_cvi": 28.36046483,
                "x_cap": -15.4147164,
                "y_cap": 28.10082028,
                "st_area_sh": 0.37454335906,
                "st_length_": 6.24154282603
            }
        },
        "centroid": [-14.546977440514675, 28.36589401112925]
    }


'''


layers_data = '''
## /layers.data

Data endpoint. This endpoint can be used to download multiple datasets, each one associated to a given layer.<br>
Each dataset has a unique identifier named <b>ev</b>.<br>
Each dataset has been collected from an official (original) source. More information about this can be found in the collection <b>provenance</b>.<br>

<b>COVID-19 datasets:</b>

* catsalut
    - <b>09.covid_abs</b> : Casos de corononavirus por ABS
    - <b>09.covid_cumun</b> : Casos de coronavirus por municipio
    - <b>09.pop_abs</b> : Distribución población por ABS
    - <b>09.deceased_shire</b> : Muertes por coronavirus por comarca

* saludmadrid
    - <b>13.covid_abs</b> : Casos de corononavirus por ABS
    - <b>13.covid_mundis</b> : Casos de coronavirus por combinación municipio-distrito

* osakidetza
    - <b>16.covid_abs</b> : Casos de corononavirus por ABS
    - <b>16.covid_cumun</b> : Casos de coronavirus por municipio
    - <b>16.covid_hosp</b> : Casos de coronavirus por hospital

* saludnavarra
    - <b>15.covid_abs</b> : Casos de corononavirus por ABS

* sacyl
    - <b>07.covid_abs</b> : Casos de corononavirus por ABS
    - <b>07.covid_hosp</b> : Casos de coronavirus por hospital
    - <b>07.deceased_abs</b> : Defunciones por corononavirus por ABS

* saludaragon
    - <b>02.covid_hosp</b> : Casos de coronavirus por hospital
    - <b>02.covid_cpro</b> : Casos de coronavirus por provincia

* d_saludnavarra
    - <b>15.covid_cp</b> : Casos de corononavirus por código postal

* cnecovid
    - <b>ES.covid_cpro</b> : Casos de coronavirus por provincia
    - <b>ES.covid_cca</b> : Casos de coronavirus por comunidad autónoma

* scsalud
    - <b>06.covid_cumun</b> : Casos de corononavirus por municipio

* saludcvalenciana
    - <b>10.covid_cumun</b> : Casos de corononavirus por municipio



<b>Mortality datasets:</b>

* MoMo
    - <b>ES.MoMo_cca</b> : Exceso de mortalidad por comunidad autónoma


<b>Lockdown datasets:</b>

* confinamientos
    - <b>ES.confinements</b> : Confinamientos en el territorio español (incomplete)


<b>Population datasets:</b>

* rel
    - <b>rel.pop_cumun</b> : Población y densidad poblacional por municipio

<br>

Example for querying one dataset:

    https://flowmaps.life.bsc.es/api/layers.data?where={"ev":"ES.covid_cpro"}

    https://flowmaps.life.bsc.es/api/layers.data?where={"ev": "ES.covid_cpro", "evstart": {"$gte": "Sat, 10 Oct 2020 00:00:00 GMT", "$lte": "Mon, 12 Oct 2020 00:00:00 GMT"}}

Example document:

    {
        "_id": {
            "$oid": "60040e6ed592fe7011e92131"
        },
        "id": "403",
        "ev": "09.covid_abs",
        "layer": "abs_09",
        "evstart": {
            "$date": "2021-01-15T23:00:00.000Z"
        },
        "evend": {
            "$date": "2021-01-16T23:00:00.000Z"
        },
        "d": {
            "cases": 2,
            "suspects": 0,
            "negative": 0,
            "stats": [{
                "diag": "casesFastAntigenTest",
                "sex": "M",
                "cases": 1
            }, {
                "diag": "casesFastAntigenTest",
                "sex": "F",
                "cases": 1
            }]
        },
        "c": {
            "absname": "BARCELONA 8-L",
            "abscode": "403",
            "sector": "BARCELONA NOU BARRIS",
            "seccode": "7854",
            "region": "BARCELONA CIUTAT",
            "regcode": "7803",
            "cca": "09"
        }
    }

'''


layers_data_consolidated = '''
## /layers.data.consolidated

Consolidated data endpoint. This endpoint can be used to download multiple datasets, each one associated to a given layer.<br>
Each dataset has a unique identifier named <b>ev</b>.<br>
Each dataset has been collected from an official (original) source. More information about this can be found in the collection <b>provenance</b>.<br>

Data in this collection come from <b>layers.data</b> but has been processed to ensure all different datasets share a common format, missing data has been corrected and has followed an automated data cleaning process.

<b>COVID-19 data:</b>

<ul>
    <li>COVID-19 data for autonomous communities: <b>ES.covid_cca</b></li>
    <li>COVID-19 data for provinces: <b>ES.covid_cpro</b></li>
    <li>Asturias COVID-19 data for municipalities: <b>03.covid_cumun</b></li>
    <li>Cantabria COVID-19 data for municipalities: <b>06.covid_cumun</b></li>
    <li>Castilla y León COVID-19 data for basic health areas: <b>07.covid_abs</b></li>
    <li>Cataluña COVID-19 data for basic health areas: <b>09.covid_abs</b></li>
    <li>Valencia COVID-19 data for municipalities: <b>10.covid_cumun</b></li>
    <li>Madrid COVID-19 data for basic health areas: <b>13.covid_abs</b></li>
    <li>Navarra COVID-19 data for basic health areas: <b>15.covid_abs</b></li>
    <li>Euskadi COVID-19 data for basic health areas: <b>16.covid_abs</b></li>
</ul>

Examples:

    https://flowmaps.life.bsc.es/api/layers.data.consolidated?where={"type":"covid19","ev":"ES.covid_cpro"}

    https://flowmaps.life.bsc.es/api/layers.data.consolidated?where={"type":"covid19","ev":"ES.covid_cpro","date":{"$gte":"2020-10-01","$lte":"2020-10-10"}}


Example document:
    
    {
        "_id": {
            "$oid": "600593c398c27cd95d189374"
        },
        "id": "403",
        "date": "2020-03-07",
        "ev": "09.covid_abs",
        "layer": "abs_09",
        "type": "covid19",
        "was_missing": false,
        "d": {
            "cases": 1,
            "suspects": 0,
            "negative": 0,
            "stats": [{
                "diag": "casesPCR",
                "sex": "M",
                "cases": 1
            }]
        },
        "c": {
            "absname": "BARCELONA 8-L",
            "abscode": "403",
            "sector": "BARCELONA NOU BARRIS",
            "seccode": "7854",
            "region": "BARCELONA CIUTAT",
            "regcode": "7803",
            "cca": "09"
        },
        "new_cases": 1,
        "total_cases": 1,
        "active_cases_7": 1,
        "active_cases_14": 1,
        "new_cases_mean_7": 0.14285714285714285,
        "new_cases_mean_14": 0.07142857142857142,
        "max_new_cases": 1,
        "max_new_cases_date": "2020-03-07",
        "updated_at": {
            "$date": "2021-01-18T14:57:04.954Z"
        }
    }


<br>


<b>Population data:</b>

This endpoint can also be used to download population data for multiple layers.

This collection contains population data at a daily basis, which has been estimated using mobility data provided by MITMA and stored in mitma_mov.zone_movements.

Contains population for the following layers:

- Mobility areas: mitma_mov
- Autonomous Communities: cnig_ccaa
- Provinces: cnig_provincias
- Cataluña: abs_09
- Castilla y León: zbs_07
- Navarra: zbs_15
- Madrid: zon_bas_13
- Euskadi: oe_16



Examples:

    https://flowmaps.life.bsc.es/api/layers.data.consolidated?where={"type":"population","layer":"cnig_provincias"}


Example document:
    
    {
        "_id": {
            "$oid": "601228984eab96573e591c15"
        },
        "date": "2021-01-24",
        "id": "35",
        "layer": "cnig_provincias",
        "population": 1082583.0991480001,
        "type": "population",
        "updated_at": {
            "$date": "2021-01-28T03:59:34.417Z"
        }
    }




'''

layers_overlaps = '''
## /layers.overlaps

This endpoint provides access to the geographic overlaps between several pairs of layers.
The overlaps can be used to convert (project) data from one layer to another.

Each entry follows the scheme:

    {
        l: {
            id: The geographical feature id within layer L
            layer: The layer L
            ratio: The ratio of area from this feature overlapping with the other one
        },
        m: {
            id: The geographical feature id within layer M
            layer: The layer M
            ratio: The ratio of area from this feature overlapping with the other one
        },
    }

Note: Unless you inspect the provenance, you cannot know in advance which layer was chosen as 'l' and which layer was chosen as 'm'

Query examples:

    https://flowmaps.life.bsc.es/api/layers.overlaps?where={"l.layer":"cnig_provincias","m.layer":"mitma_mov"}


'''



layers_overlaps_population = '''
## /layers.overlaps_population

This endpoint provides access to the population overlaps between several pairs of layers.
The overlaps can be used to convert (project) data from one layer to another.

Each entry follows the scheme:

    {
        l: {
            id: The geographical feature id within layer L
            layer: The layer L
            ratio: The ratio of population from this feature shared with the other one
        },
        m: {
            id: The geographical feature id within layer M
            layer: The layer M
            ratio: The ratio of population from this feature shared with the other one
        },
    }


Query examples:

    https://flowmaps.life.bsc.es/api/layers.overlaps_population?where={"l.layer":"cnig_provincias","m.layer":"mitma_mov"}


'''


mitma_mov_movements_raw = '''
## /mitma_mov.movements_raw

MITMA dataset (https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data)

Contains hourly Origin-Destination data, based on anonymized mobile phone records, processed and provided by MITMA.

The data in this collection is associated with the layer <b>mitma_mov</b>: the fields **origen** and **destino** contains ids from that layer.

The fields **role_origen** and **role_destino** contains one of the following values: `casa`, `trabajo`, `otros`


Example queries:

    https://flowmaps.life.bsc.es/api/mitma_mov.movements_raw?where={"origen":"08064_AM","destino": "08031_AM", "role_origen": "casa", "role_destino": "trabajo"}

    https://flowmaps.life.bsc.es/api/mitma_mov.movements_raw?where={"origen":"01001_AM","evstart": {"$gte": "Sat, 10 Oct 2020 00:00:00 GMT", "$lte": "Mon, 12 Oct 2020 00:00:00 GMT"}}


Note: This collection is HUGE and the queries may be very slow. Try to query small date ranges or, as an alternative, we have calculated daily aggregations which are available at the endpoint: **daily_mobility_matrix**.


Example document:
    
    {
        "_id": {
            "$oid": "5ee9545593561c9bd401e786"
        },
        "evstart": {
            "$date": "2020-05-20T22:00:00.000Z"
        },
        "evend": {
            "$date": "2020-05-20T23:00:00.000Z"
        },
        "evday": {
            "$date": "2020-05-20T22:00:00.000Z"
        },
        "origen": "01001_AM",
        "destino": "01001_AM",
        "role_origen": "casa",
        "role_destino": "otros",
        "residencia": "01",
        "distancia_min": 2,
        "distancia_max": 5,
        "viajes": 7.571,
        "viajes_km": 24.498
    }



'''



mitma_mov_zone_movements = '''
## /mitma_mov.zone_movements

MITMA dataset (https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data)

Contains the number of people in each mobility area (**mitma_mov**) that has done 0,1,2,3+ trips. NOTE: 3 or more trips are encoded as 'Inf'.

Example document:

    {
        "_id": {
            "$oid": "5fac79de2fa94137bee019d7"
        },
        "id": "01001_AM",
        "layer": "mitma_mov",
        "evstart": {
            "$date": "2020-11-06T23:00:00.000Z"
        },
        "evend": {
            "$date": "2020-11-07T23:00:00.000Z"
        },
        "viajes": 0,
        "personas": 5119.027
    }

Example query:

    https://flowmaps.life.bsc.es/api/mitma_mov.movements_raw?where={"origen":"01001_AM","evstart": {"$gte": "Sat, 10 Oct 2020 00:00:00 GMT", "$lte": "Mon, 12 Oct 2020 00:00:00 GMT"}}

'''



daily_mobility_matrix = '''
## /mitma_mov.daily_mobility_matrix
    
Contains daily aggregations of the data stored in `mitma_mov.movements_raw`.

Contains daily Origin-Destination number of trips, based on anonymized mobile phone records, processed and provided by MITMA (https://www.mitma.gob.es/ministerio/covid-19/evolucion-movilidad-big-data).

While the `mitma_mov.movements_raw` collection contains data only for the layer **mitma_mov**, this collection contains data for multiple layers, and also multiple pairs of source and target layers (for example: daily number of trips from each area in **cnig_provincias** to each area in **abs_09**).

Example queries:

    https://flowmaps.life.bsc.es/api/mitma_mov.daily_mobility_matrix?where={"source_layer": "cnig_provincias", "target_layer": "cnig_provincias", "source":"28", "target": "08"}

    https://flowmaps.life.bsc.es/api/mitma_mov.daily_mobility_matrix?where={"source_layer": "cnig_provincias", "target_layer": "cnig_provincias", "date": "2020-10-10"}


Example document:

    {
        "_id": {
            "$oid": "5ff91f128e6334e0cc47e536"
        },
        "source": "28",
        "target": "09",
        "trips": 1773.1847776549996,
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "date": "2021-01-05",
        "updated_at": {
            "$date": "2021-01-09T04:12:17.942Z"
        }
    }


Available pairs of (source_layer, target_layer):

    {"source_layer": "mitma_mov", "target_layer": "mitma_mov"},
    {"source_layer": "cnig_provincias", "target_layer": "cnig_provincias"},
    {"source_layer": "cnig_ccaa", "target_layer": "cnig_ccaa"},
    {"source_layer": "abs_09", "target_layer": "abs_09"},
    {"source_layer": "zbs_15", "target_layer": "zbs_15"},
    {"source_layer": "zbs_07", "target_layer": "zbs_07"},
    {"source_layer": "oe_16", "target_layer": "oe_16"},
    {"source_layer": "zon_bas_13", "target_layer": "zon_bas_13"},
    {"source_layer": "cnig_provincias", "target_layer": "abs_09"},
    {"source_layer": "abs_09", "target_layer": "cnig_provincias"},
    {"source_layer": "cnig_provincias", "target_layer": "zbs_15"},
    {"source_layer": "zbs_15", "target_layer": "cnig_provincias"},
    {"source_layer": "cnig_provincias", "target_layer": "zbs_07"},
    {"source_layer": "zbs_07", "target_layer": "cnig_provincias"},
    {"source_layer": "cnig_provincias", "target_layer": "oe_16"},
    {"source_layer": "oe_16", "target_layer": "cnig_provincias"},
    {"source_layer": "cnig_provincias", "target_layer": "zon_bas_13"},
    {"source_layer": "zon_bas_13", "target_layer": "cnig_provincias}


'''



provenance = '''
## /provenance

Each time a dataset is stored in the database an entry is stored in this collection. It helps to clean up old contents when a collection contains more than one (maybe anonymous) dataset, like it happens with layers and layers.data.

Each entry follows the scheme:

    {
        storedBy: A string defining the program which stored the dataset
        storedIn: The name of the database collection where the dataset was stored
        storedAt: The timestamp where the dataset started to be stored
        lastStoredAt: The timestamp where the dataset finished to be stored
        numEntries: The number of inserted entries
        fetched: [
            An array of objects following next structure
            {
                from: Where the dataset was fetched from. It can be either a local, unportable path , a ‘collection’ URL (when the entries were generated from an aggregation pipeline) or an HTTP(S) or FTP URL
                by: A string defining the program which fetched the dataset
                at: The timestamp of the dataset. In the case of online datasets, it is when it was fetched. In the case of collections, it is when its contents were generated. In the case of local datasets, the filesystem timestamp
                originalChecksum: The SHA256 of the original, raw content (not used for source collections)
            }
        ],
        keywords: An object which contains keywords useful in different scenarios. Examples: the name of the geographical feature attribute within a layer used to name the feature (‘nameKey’), the description of the layer (‘layerDesc’ key), etc...
    }

Example query:

    https://flowmaps.life.bsc.es/api/provenance?where={"storedIn":"layers.data","keywords.ev":"ES.covid_cpro"}

Example document:

    {
        "_id": {
            "$oid": "5efa87e6d5890e82745d5abd"
        },
        "storedAt": {
            "$date": "2020-06-30T00:31:34.013Z"
        },
        "storedIn": "layers.data",
        "storedBy": "dataset-fetcher.py",
        "fetched": [{
            "from": "https://cnecovid.isciii.es/covid19/resources/datos_ccaas.csv",
            "at": {
                "$date": "2020-06-30T00:30:54.114Z"
            },
            "originalChecksum": "sha256=Frqvq1e18QQSceYdO0ILGWcGpkceeVn0XKJYu3TFS2Y=",
            "by": "dataset-fetcher.py"
        }],
        "numEntries": 0,
        "keywords": {
            "ev": "ES.covid_cpro",
            "layer": "cnig_ccaa",
            "evDesc": "Casos de coronavirus por comunidad autónoma"
        }
    }

'''



