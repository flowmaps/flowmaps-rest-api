import pytz
from datetime import datetime

tz = pytz.timezone('Europe/Madrid')

specs = [
    {
        'name': 'MITMA mobility: movements_raw',
        'collection': 'mitma_mov.movements_raw',
        'filters': {
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': 'MITMA mobility: zone_movements',
        'collection': 'mitma_mov.zone_movements',
        'filters': {
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': 'MITMA mobility: daily_mobility_matrix (extracted from mitma_mov.movements_raw)',
        'collection': 'mitma_mov.daily_mobility_matrix',
        'filters': {
            'source_layer': 'cnig_provincias',
            'target_layer': 'cnig_provincias'
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },

    # population
    {
        'name': 'Population (extracted from mitma_mov.zone_movements)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'type': 'population',
            'layer': 'cnig_provincias',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },

    # layers.data
    {
        'name': 'ES.covid_cpro',
        'collection': 'layers.data',
        'filters': {
            'ev': 'ES.covid_cpro'
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': 'ES.covid_cca',
        'collection': 'layers.data',
        'filters': {
            'ev': 'ES.covid_cca'
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': '09.covid_abs',
        'collection': 'layers.data',
        'filters': {
            'ev': '09.covid_abs',
            'layer': 'abs_09',
            'id': '100',
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': '13.covid_abs',
        'collection': 'layers.data',
        'filters': {
            'ev': '13.covid_abs',
            'layer': 'zon_bas_13',
            'id': '080',
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': '07.covid_abs',
        'collection': 'layers.data',
        'filters': {
            'ev': '07.covid_abs',
            'layer': 'zbs_07',
            'id': '170209',
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': '16.covid_abs',
        'collection': 'layers.data',
        'filters': {
            'ev': '16.covid_abs',
            'layer': 'oe_16',
            'id': '1107',
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    {
        'name': '15.covid_abs',
        'collection': 'layers.data',
        'filters': {
            'ev': '15.covid_abs',
            'layer': 'zbs_15',
            'id': '46',
        },
        'projection': {
            'evstart': -1,
            '_id': 0
        },
        'datefield': 'evstart',
    },
    
    # layers.data.consolidated
    {
        'name': 'Covid data for Provinces (ES.covid_cpro consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': 'ES.covid_cpro',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
    {
        'name': 'Covid data for CCAA (ES.covid_cca consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': 'ES.covid_cca',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
    {
        'name': 'Covid data for Cataluña (09.covid_abs consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': '09.covid_abs',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
    {
        'name': 'Covid data for Madrid (13.covid_abs consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': '13.covid_abs',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
    {
        'name': 'Covid data for Castilla y León (07.covid_abs consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': '07.covid_abs',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
    {
        'name': 'Covid data for Euskadi (16.covid_abs consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': '16.covid_abs',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
    {
        'name': 'Covid data for Navarra (15.covid_abs consolidated)',
        'collection': 'layers.data.consolidated',
        'filters': {
            'ev': '15.covid_abs',
            'type': 'covid19',
        },
        'projection': {
            'date': -1,
            '_id': 0,
            'updated_at': 1,
        },
        'datefield': 'date',
        'processed': True,
    },
]


def monitor_collection(db, spec):
    cursor = db[spec['collection']].find(spec['filters'], spec['projection']).sort(spec['datefield'], -1).limit(1)
    docs = list(cursor)

    if not docs:
        return {'spec': spec, 'last_avail_date': 'error'}

    date = docs[0][spec['datefield']]

    if isinstance(date, datetime):
        date = date.astimezone(tz).strftime('%Y-%m-%d')

    delay = int((datetime.now() - datetime.strptime(date, '%Y-%m-%d')).days)

    return {'spec': spec, 'last_avail_date': date, 'raw': docs[0], 'delay': delay}


def monitor_all(db):

    results = []

    for spec in specs:
        print(spec['name'])
        res = monitor_collection(db, spec)
        results.append(res)

    return {'_items': results}

