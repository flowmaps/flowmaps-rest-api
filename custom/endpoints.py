import functools
import pymongo
import pytz
import inspect
import json
import pandas as pd
import datetime
from flask import request
tz = pytz.timezone('Europe/Madrid')


def init_mongodb(mongodb_params, database_name='FlowMaps'):
    client = pymongo.MongoClient(**mongodb_params)
    codec_options = client.codec_options.with_options(tzinfo=tz, tz_aware=True)
    db = client.get_database(database_name, codec_options=codec_options)
    return db


def check_parameters(func):
    @functools.wraps(func)
    def wrapper(db):
        try:
            params = json.loads(request.args.get('where', '{}'))
        except Exception as e:
            print('ERROR:', e)
            return {'error': 'unable to parse query'}
        parameters = inspect.signature(func).parameters
        needed = [k for k in parameters if parameters[k].default is inspect._empty and k not in ['db']]
        optional = [k for k in parameters if parameters[k].default is not inspect._empty]
        unexpected = [k for k in params if k not in needed+optional]
        missing = [k for k in needed if k not in params]
        if missing:
            return {'error': f"Missing needed parameters: {missing}"}
        if unexpected:
            return {'error': f"Unexpected parameters: {unexpected}. Accepted parameters: {needed+optional}"}
        return func(db, **params)
    return wrapper


def check_exist_registers(db, collection, filters, date):
    cursor = db[collection].find({**filters, 'date': date}).limit(1)
    return list(cursor) != []


def get_risk_dates(db):
    # Mobility queries
    cursor = db['mitma_mov.daily_mobility_matrix'].find(
        {'source_layer': 'cnig_provincias', 'target_layer': 'cnig_provincias'},
        {'date': -1, '_id': 0}
    ).sort('date', -1).limit(1)
    mobility_max_date = next(cursor)['date']
    cursor = db['mitma_mov.daily_mobility_matrix'].find(
        {'source_layer': 'cnig_provincias', 'target_layer': 'cnig_provincias'},
        {'date': -1, '_id': 0}
    ).sort('date', 1).limit(1)
    mobility_min_date = next(cursor)['date']

    # Covid queries
    cursor = db['layers.data.consolidated'].find(
        {'type': 'covid19', 'ev': 'ES.covid_cpro'},
        {'date': -1, '_id': 0}
    ).sort('date', -1).limit(1)
    covid_max_date = next(cursor)['date']
    cursor = db['layers.data.consolidated'].find(
        {'type': 'covid19', 'ev': 'ES.covid_cpro'},
        {'date': -1, '_id': 0}
    ).sort('date', 1).limit(1)
    covid_min_date = next(cursor)['date']

    # Population queries
    cursor = db['layers.data.consolidated'].find(
        {'type': 'population', 'layer': 'cnig_provincias'},
        {'date': -1, '_id': 0}
    ).sort('date', -1).limit(1)
    pop_max_date = next(cursor)['date']
    cursor = db['layers.data.consolidated'].find(
        {'type': 'population', 'layer': 'cnig_provincias'},
        {'date': -1, '_id': 0}
    ).sort('date', 1).limit(1)
    pop_min_date = next(cursor)['date']

    min_date = max(mobility_min_date, covid_min_date, pop_min_date)
    max_date = min(mobility_max_date, covid_max_date, pop_max_date)

    iterations = 1
    while iterations < 100:
        mobility_check = check_exist_registers(
            db,
            'mitma_mov.daily_mobility_matrix',
            {'source_layer': 'cnig_provincias', 'target_layer': 'cnig_provincias'},
            max_date,
        )
        population_check = check_exist_registers(
            db,
            'layers.data.consolidated',
            {'layer': 'cnig_provincias', 'type': 'population'},
            max_date
        )
        covid_check = check_exist_registers(
            db,
            'layers.data.consolidated',
            {'type': 'covid19', 'ev': 'ES.covid_cpro'},
            max_date,
        )
        if mobility_check and population_check and covid_check:
            return {'min_date': min_date, 'max_date': max_date}

        max_date = (
                datetime.datetime.strptime(max_date, '%Y-%m-%d') - datetime.timedelta(days=1)
        ).strftime('%Y-%m-%d')
        iterations += 1


def get_incidence_dates(db):
    # Covid queries
    cursor = db['layers.data.consolidated'].find(
        {'type': 'covid19', 'ev': 'ES.covid_cpro'},
        {'date': -1, '_id': 0}
    ).sort('date', -1).limit(1)
    covid_max_date = next(cursor)['date']
    cursor = db['layers.data.consolidated'].find(
        {'type': 'covid19', 'ev': 'ES.covid_cpro'},
        {'date': -1, '_id': 0}
    ).sort('date', 1).limit(1)
    covid_min_date = next(cursor)['date']

    min_date = covid_min_date
    max_date = covid_max_date
    return {'min_date': min_date, 'max_date': max_date}


@check_parameters
def get_outgoing_risk(db, source, date, ev, source_layer, target_layer, exclude_same=True):
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'source': source, 'date': date}
    mobility = list(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0}))
    if not mobility:
        return {"error": f"no mobility data found matching: {filters}"}
    
    filters = {'type': 'covid19', 'ev': ev, 'id': source, 'date': date}
    cases = db['layers.data.consolidated'].find_one(filters)
    if not cases:
        return {"error": f"no covid data found matching: {filters}"}
    
    filters = {'type': 'population', 'layer': source_layer, 'id': source, 'date': date}
    doc = db['layers.data.consolidated'].find_one(filters)
    if not doc:
        return {"error": f"no population data found matching: {filters}"}
    pop = doc['population']
    
    docs = mobility
    for doc in docs:
        doc['source_active_cases_14'] = cases['active_cases_14']
        doc['source_active_cases_7'] = cases['active_cases_7']
        doc['source_new_cases'] = cases['new_cases']
        doc['source_population'] = pop
        doc['outgoing_risk'] = (cases['active_cases_14']/pop) * doc['trips']
    if exclude_same:
        docs = [d for d in docs if d['source'] != d['target']]
    return {'_items': docs}


@check_parameters
def get_outgoing_risk_multiple_sources(db, sources, date, ev, source_layer, target_layer, exclude_same=True):
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'source': {"$in": sources}, 'date': date}
    docs = list(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0}))
    if not docs:
        return {"error": f"no mobility data found matching: {filters}"}
    mobility = {(d['source'], d['target']): d for d in docs}
    targets = set(d['target'] for d in docs)
    if exclude_same:
        targets = targets.difference(sources)

    filters = {'type': 'covid19', 'ev': ev, 'id': {'$in': sources}, 'date': date}
    docs = list(db['layers.data.consolidated'].find(filters))
    if not docs:
        return {"error": f"no covid data found matching: {filters}"}
    cases = {d['id']: d for d in docs}
    
    filters = {'type': 'population', 'layer': source_layer, 'id': {'$in': sources}, 'date': date}
    docs = list(db['layers.data.consolidated'].find(filters))
    if not docs:
        return {"error": f"no population data found matching: {filters}"}
    pop = {d['id']: d['population'] for d in docs}

    data = {target: {'source_population': 0,
                     'source_new_cases': 0,
                     'source_active_cases_7': 0,
                     'source_active_cases_14': 0,
                     'outgoing_risk': 0,
                     'sources': [],
                     'trips': 0,
                     'target': target,
                    } for target in targets}
    for (source, target), d in mobility.items():
        if target not in targets:
            continue
        data[target]['source_population'] += pop[source]
        data[target]['source_new_cases'] += cases[source]['new_cases']
        data[target]['source_active_cases_14'] += cases[source]['active_cases_14']
        data[target]['source_active_cases_7'] += cases[source]['active_cases_7']
        data[target]['trips'] += d['trips']
        data[target]['sources'].append(source)
        data[target]['outgoing_risk'] += (cases[source]['active_cases_14']/pop[source]) * d['trips']

    # Compute risk using totals
    # NOTE: the risk can be calculated source by source, or aggregating all the
    #  source's cases, population and trips and calculating it with those totals
    # This is due to the difference in the calculation of the proportion of infected 
    # When there is just one source, both ways of computing risk are equivalent

    for target, d in data.items():
        d['outgoing_risk_computed_with_totals'] = (d['source_active_cases_14']/d['source_population']) * d['trips']

    return {'_items': list(data.values())}


@check_parameters
def get_incoming_risk(db, target, date, ev, source_layer, target_layer, exclude_same=True):
    # http://127.0.0.1:8081/incoming_risk?where={"source_layer": "cnig_provincias", "target_layer": "cnig_provincias", "date": "2020-06-01", "target": "08", "ev": "ES.covid_cpro"}
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'target': target, 'date': date}
    mobility_df = pd.DataFrame(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0}))
    if mobility_df.empty:
        return {"error": f"no mobility data found matching: {filters}"}

    filters = {'type': 'covid19', 'ev': ev, 'date': date}
    covid_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'_id': 0}))
    if covid_df.empty:
        return {"error": f"no covid data found matching: {filters}"}
    covid = covid_df.set_index('id').to_dict('index')

    filters = {'type': 'population', 'layer': source_layer, 'date': date}
    pop_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'_id': 0}))
    if pop_df.empty:
        return {"error": f"no population data found matching: {filters}"}
    pop = pop_df.set_index('id')['population'].to_dict()
    
    df = mobility_df
    df = df.drop('updated_at', 1)
    df['source_active_cases_14'] = df['source'].apply(lambda x: covid.get(x, {}).get('active_cases_14', 0))
    df['source_active_cases_7'] = df['source'].apply(lambda x: covid.get(x, {}).get('active_cases_7', 0))
    df['source_new_cases'] = df['source'].apply(lambda x: covid.get(x, {}).get('new_cases', 0))
    df['source_population'] = df['source'].apply(pop.get)
    df['incoming_risk'] = df['trips'] * df['source_active_cases_14']/df['source_population']

    if exclude_same:
        df = df[df['source']!=df['target']]

    return {'_items': df.to_dict('records')}


@check_parameters
def get_incoming_risk_multiple_targets(db, targets, date, ev, source_layer, target_layer, exclude_same=True):
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'target': {"$in": targets}, 'date': date}
    docs = list(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0}))
    if not docs:
        return {"error": f"no mobility data found matching: {filters}"}
    mobility = {(d['source'], d['target']): d for d in docs}
    sources = set(d['source'] for d in docs)
    if exclude_same:
        sources = sources.difference(targets)

    filters = {'type': 'covid19', 'ev': ev, 'id': {'$in': list(sources)}, 'date': date}
    docs = list(db['layers.data.consolidated'].find(filters))
    if not docs:
        return {"error": f"no covid data found matching: {filters}"}
    cases = {d['id']: d for d in docs}
    
    filters = {'type': 'population', 'layer': source_layer, 'id': {'$in': list(sources)}, 'date': date}
    docs = list(db['layers.data.consolidated'].find(filters))
    if not docs:
        return {"error": f"no population data found matching: {filters}"}
    pop = {d['id']: d['population'] for d in docs}

    data = {source: {'source_population': pop[source],
                     'source_new_cases': cases[source]['new_cases'],
                     'source_active_cases_7': cases[source]['active_cases_7'],
                     'source_active_cases_14': cases[source]['active_cases_14'],
                     'incoming_risk': 0,
                     'trips': 0,
                     'targets': [],
                     'source': source,
                    } for source in sources}
    for (source, target), d in mobility.items():
        if source not in sources:
            continue
        data[source]['trips'] += d['trips']
        data[source]['targets'].append(target)
        data[source]['incoming_risk'] += (cases[source]['active_cases_14']/pop[source]) * d['trips']

    # Compute risk using totals
    # NOTE: should be the same
    for source, d in data.items():
        d['outgoing_risk_computed_with_totals'] = (d['source_active_cases_14']/d['source_population']) * d['trips']

    return {'_items': list(data.values())}


@check_parameters
def get_aggregated_incoming_risk(db, date, ev, source_layer, target_layer, exclude_same=True):
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'date': date}
    mobility_df = pd.DataFrame(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0}))
    if mobility_df.empty:
        return {"error": f"no mobility data found matching: {filters}"}

    filters = {'type': 'covid19', 'ev': ev, 'date': date}
    covid_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'_id': 0}))
    if covid_df.empty:
        return {"error": f"no covid data found matching: {filters}"}
    covid = covid_df.set_index('id').to_dict('index')

    filters = {'type': 'population', 'layer': source_layer, 'date': date}
    pop_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'_id': 0}))
    if pop_df.empty:
        return {"error": f"no population data found matching: {filters}"}
    pop = pop_df.set_index('id')['population'].to_dict()
    
    df = mobility_df
    df = df.drop('updated_at', 1)
    df['source_active_cases_14'] = df['source'].apply(lambda x: covid.get(x, {}).get('active_cases_14', 0))
    df['source_active_cases_7'] = df['source'].apply(lambda x: covid.get(x, {}).get('active_cases_7', 0))
    df['source_new_cases'] = df['source'].apply(lambda x: covid.get(x, {}).get('new_cases', 0))
    df['source_population'] = df['source'].apply(pop.get)
    df['incoming_risk'] = df['trips'] * df['source_active_cases_14']/df['source_population']

    if exclude_same:
        df = df[df['source']!=df['target']]

    df = df.groupby('target')[['incoming_risk', 'trips']].sum().reset_index()
    df = df.rename(columns={'target': 'id', 'trips': 'incoming_trips'})
    return {'_items': df.to_dict('records')}


@check_parameters
def get_aggregated_outgoing_risk(db, date, ev, source_layer, target_layer, exclude_same=True):
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'date': date}
    mobility_df = pd.DataFrame(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0}))
    if mobility_df.empty:
        return {"error": f"no mobility data found matching: {filters}"}

    filters = {'type': 'covid19', 'ev': ev, 'date': date}
    covid_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'_id': 0}))
    if covid_df.empty:
        return {"error": f"no covid data found matching: {filters}"}
    covid = covid_df.set_index('id').to_dict('index')

    filters = {'type': 'population', 'layer': source_layer, 'date': date}
    pop_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'_id': 0}))
    if pop_df.empty:
        return {"error": f"no population data found matching: {filters}"}
    pop = pop_df.set_index('id')['population'].to_dict()
    
    df = mobility_df
    df = df.drop('updated_at', 1)
    df['source_active_cases_14'] = df['source'].apply(lambda x: covid.get(x, {}).get('active_cases_14', 0))
    df['source_active_cases_7'] = df['source'].apply(lambda x: covid.get(x, {}).get('active_cases_7', 0))
    df['source_new_cases'] = df['source'].apply(lambda x: covid.get(x, {}).get('new_cases', 0))
    df['source_population'] = df['source'].apply(pop.get)
    df['outgoing_risk'] = df['trips'] * df['source_active_cases_14']/df['source_population']

    if exclude_same:
        df = df[df['source']!=df['target']]

    df = df.groupby('source')[['outgoing_risk', 'trips']].sum().reset_index()
    df = df.rename(columns={'source': 'id', 'trips': 'outgoing_trips'})
    return {'_items': df.to_dict('records')}


@check_parameters
def get_incoming_risk_history(db, target, ev, source_layer, target_layer, start_date=None, end_date=None, total=False, exclude_same=True):
    # http://127.0.0.1:8081/incoming_risk_history?where={"source_layer":"cnig_provincias","target_layer":"cnig_provincias","target":"08","ev":"ES.covid_cpro","total":false}
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'target': target}
    if start_date and end_date:
        filters['date'] = {"$gte": start_date, "$lte": end_date}
    elif start_date:
        filters['date'] = {"$gte": start_date}
    elif end_date:
        filters['date'] = {"$lte": end_date}
    mobility_df = pd.DataFrame(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0, 'updated_at': 0}))
    if mobility_df.empty:
        return {"error": f"no mobility data found matching: {filters}"}

    filters = {'type': 'covid19', 'ev': ev}
    if start_date and end_date:
        filters['date'] = {"$gte": start_date, "$lte": end_date}
    elif start_date:
        filters['date'] = {"$gte": start_date}
    elif end_date:
        filters['date'] = {"$lte": end_date}
    covid_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'id': 1, 'date': 1, 'active_cases_14': 1}))
    if covid_df.empty:
        return {"error": f"no covid data found matching: {filters}"}
    covid = covid_df.set_index(['id', 'date'])['active_cases_14'].to_dict()

    filters = {'type': 'population', 'layer': source_layer}
    if start_date and end_date:
        filters['date'] = {"$gte": start_date, "$lte": end_date}
    elif start_date:
        filters['date'] = {"$gte": start_date}
    elif end_date:
        filters['date'] = {"$lte": end_date}
    pop_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'id': 1, 'date': 1, 'population': 1}))
    if pop_df.empty:
        return {"error": f"no population data found matching: {filters}"}
    pop = pop_df.set_index(['id', 'date'])['population'].to_dict()

    df = mobility_df
    df['source_active_cases_14'] = df.apply(lambda r: covid.get((r['source'], r['date'])), axis=1)
    df['source_population'] = df.apply(lambda r: pop.get((r['source'], r['date'])), axis=1)
    df['incoming_risk'] = df['trips'] * df['source_active_cases_14']/df['source_population']
    df = df.dropna(how='any')
    if exclude_same:
        df = df[df['source']!=df['target']]
    if total:
        df = df.groupby('date')['incoming_risk'].sum().reset_index()
    return {'_items': df.to_dict('records')}


@check_parameters
def get_outgoing_risk_history(db, source, ev, source_layer, target_layer, start_date=None, end_date=None, total=False, exclude_same=True):
    # http://127.0.0.1:8081/outgoing_risk_history?where={"source_layer":"cnig_provincias","target_layer":"cnig_provincias","source":"08","ev":"ES.covid_cpro","total":false}
    filters = {'source_layer': source_layer, 'target_layer': target_layer, 'source': source}
    if start_date and end_date:
        filters['date'] = {"$gte": start_date, "$lte": end_date}
    elif start_date:
        filters['date'] = {"$gte": start_date}
    elif end_date:
        filters['date'] = {"$lte": end_date}
    mobility_df = pd.DataFrame(db['mitma_mov.daily_mobility_matrix'].find(filters, {'_id': 0, 'updated_at': 0}))
    if mobility_df.empty:
        return {"error": f"no mobility data found matching: {filters}"}

    filters = {'type': 'covid19', 'ev': ev, 'id': source}
    if start_date and end_date:
        filters['date'] = {"$gte": start_date, "$lte": end_date}
    elif start_date:
        filters['date'] = {"$gte": start_date}
    elif end_date:
        filters['date'] = {"$lte": end_date}
    covid_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'date': 1, 'active_cases_14': 1}))
    if covid_df.empty:
        return {"error": f"no covid data found matching: {filters}"}
    covid = covid_df.set_index('date')['active_cases_14'].to_dict()

    filters = {'type': 'population', 'layer': source_layer, 'id': source}
    if start_date and end_date:
        filters['date'] = {"$gte": start_date, "$lte": end_date}
    elif start_date:
        filters['date'] = {"$gte": start_date}
    elif end_date:
        filters['date'] = {"$lte": end_date}
    pop_df = pd.DataFrame(db['layers.data.consolidated'].find(filters, {'date': 1, 'population': 1}))
    if pop_df.empty:
        return {"error": f"no population data found matching: {filters}"}
    pop = pop_df.set_index('date')['population'].to_dict()

    df = mobility_df
    df['source_active_cases_14'] = df['date'].apply(covid.get)
    df['source_population'] = df['date'].apply(pop.get)
    df['outgoing_risk'] = df['trips'] * df['source_active_cases_14']/df['source_population']
    df = df.dropna(how='any')
    if exclude_same:
        df = df[df['source']!=df['target']]
    if total:
        df = df.groupby('date')['outgoing_risk'].sum().reset_index()
    return {'_items': df.to_dict('records')}


@check_parameters
def get_incidence(db, ev, start_date=None, end_date=None):
    fields = [
        'active_cases_14', 
        'active_cases_7',
        'new_cases',
        'total_cases',
        # 'max_new_cases',
        # 'max_new_cases_date',
    ]

    if start_date and end_date:
        date_filters = {'date': {'$gte': start_date, '$lte': end_date}}
    elif start_date:
        date_filters = {'date': {'$gte': start_date}}
    elif end_date:
        date_filters = {'date': {'$lte': end_date}}
    else:
        date_filters = {}

    pipeline = [
        {
            '$match': {
                'type': 'covid19', 
                'ev': ev,
                **date_filters,
            }
        }, {
            '$project': {
                'date': 1,
                'id': 1,
                **{field: 1 for field in fields}
            }
        }, {
            '$group': {
                '_id': '$date', 
                # 'count': {
                #     '$sum': 1
                # }, 
                'data': {
                    '$push': {
                        'id': '$id', 
                        **{field: '$'+field for field in fields}
                    }
                }
            }
        }
    ]
    cursor = db['layers.data.consolidated'].aggregate(pipeline)
    return {'_items': list(cursor)}


@check_parameters
def get_aggregate(db, collection, pipeline):
    cursor = db[collection].aggregate(pipeline)
    return {'_items': list(cursor)}


@check_parameters
def get_total_incoming_daily_mobility(db, target, source_layer, target_layer, exclude_same=True):
    filters = {
        'source_layer': source_layer,
        'target_layer': target_layer,
        'target': target,
    }
    if exclude_same:
        filters['source'] = {'$ne': target}

    pipeline = [
        {
            '$match': filters
        }, {
            '$project': {
                'trips': 1,
                'date': 1,
            }
        }, {
            '$group': {
                '_id': '$date', 
                'trips': {'$sum': '$trips'}
            }
        }, {
            '$project': {
                '_id': 0,
                'date': '$_id',
                'trips': 1,
            }
        }
    ]

    cursor = db['mitma_mov.daily_mobility_matrix'].aggregate(pipeline)
    docs = sorted(cursor, key=lambda d: d['date'])
    return {'_items': docs}


@check_parameters
def get_total_outgoing_daily_mobility(db, source, source_layer, target_layer, exclude_same=True):
    filters = {
        'source_layer': source_layer,
        'target_layer': target_layer,
        'source': source,
    }
    if exclude_same:
        filters['target'] = {'$ne': source}

    pipeline = [
        {
            '$match': filters
        }, {
            '$project': {
                'trips': 1,
                'date': 1,
            }
        }, {
            '$group': {
                '_id': '$date', 
                'trips': {'$sum': '$trips'}
            }
        }, {
            '$project': {
                '_id': 0,
                'date': '$_id',
                'trips': 1,
            }
        }
    ]

    cursor = db['mitma_mov.daily_mobility_matrix'].aggregate(pipeline)
    docs = sorted(cursor, key=lambda d: d['date'])
    return {'_items': docs}


@check_parameters
def distinct(db, collection, field, query=None):
    cursor = db[collection].distinct(field, query)
    return {'_items': list(cursor)}
