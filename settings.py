import json
import os
from eve.auth import BasicAuth, TokenAuth

import descriptions

"""
Load secrets from separated file
"""
dir_path = os.path.dirname(os.path.realpath(__file__))
secrets_path = os.path.join(dir_path, 'config.secrets.json')

with open(secrets_path) as f:
    SECRETS = json.load(f)


# TODO: in the future, move this classes to a separated auth.py file, but now
# they need to look up the passwords and tokens in SECRETS 
class MitmaBasicAuth(BasicAuth):
    def check_auth(self, username, password, allowed_roles, resource,
                   method):
        #print(username, password, allowed_roles, resource, method)
        for user in SECRETS['MITMA_MOV_ALLOWED_USERS']:
            if username == user['username'] and password == user['password']:
                return True
        return False


class MitmaTokenAuth(TokenAuth):
    def check_auth(self, token, allowed_roles, resource, method):
        #print(token, allowed_roles, resource, method)
        return token in SECRETS['MITMA_MOV_ALLOWED_TOKENS']


def on_fetched_resource(resource, response):
    for document in response['_items']:
        del(document['_etag'])
        del(document['_updated'])
        del(document['_created'])
        del(document['_links'])
        

def filter_provenance_fetched(response):

    def valid_from(e):
        # avoid returning local file paths in the 'from' field
        if 'from' not in e:
            return True
        if e['from'].startswith('https://') or e['from'].startswith('http://'):
            return True
        return False

    items = []
    for doc in response['_items']:
        if 'fetched' not in doc:
            continue
        doc['fetched'] = [item for item in doc['fetched'] if valid_from(item)]


"""
Build Eve settings
"""
EVE_SETTINGS = {
    **SECRETS,
    "DEBUG": False,
    "DATE_FORMAT": "%Y-%m-%d",
    "SWAGGER_HOST": 'https://flowmaps.life.bsc.es/api',  # the ui will build the urls using this base
    # "SWAGGER_HOST": 'http://127.0.0.1:5000',  # the ui will build the urls using this base
    "PAGINATION_LIMIT": 1000,
    "RENDERERS": ["eve.render.JSONRenderer"],
    'DOMAIN': {
        'layers': {
            'description': descriptions.layers,
            'allow_unknown': True,
            'schema': {
                'id': {'type': 'string', 'description': 'unique identifier for this polygon in this layer'},
                'layer': {'type': 'string', 'description': 'layer that contains this polygon'},
                'feat': {
                    'type': 'dict',
                    'schema': {
                        'type': {'type': 'string'},
                        'geometry': {'type': 'dict'},  # this can be a Polygon, Point, ..
                        'properties': {'type': 'dict'},
                    }
                },
                'centroid': {
                    'type': 'list',
                    'schema': {'type': 'float'}
                }
            },
            'item_title': 'layers',
            # 'additional_lookup': {
            #     'url': 'regex("[\w]+:[\w]+")',
            #     'field': '_id'
            # }
        },
        'layers.data': {
            'description': descriptions.layers_data,
            'allow_unknown': True,
            'item_title': 'layers.data',
            'schema': {
                'id': {'type': 'string'},
                'ev': {'type': 'string'},
                'layer': {'type': 'string'},
                'evstart': {'type': 'datetime'},
                'evend': {'type': 'datetime'},
                'c': {'type': 'dict'},
                'd': {'type': 'dict'},
            },
        },
        'layers.data.consolidated': {
            'description': descriptions.layers_data_consolidated,
            'allow_unknown': True,
            'item_title': 'layers.data.consolidated',
            'schema': {
                'id': {'type': 'string'},
                'ev': {'type': 'string'},
                'layer': {'type': 'string'},
                'date': {'type': 'string'},
                'type': {'type': 'string'},
                'population': {'type': 'float'},
                'updated_at': {'type': 'datetime'},
                'new_cases': {'type': 'float'},
                'total_cases': {'type': 'float'},
                'active_cases_7': {'type': 'float'},
                'active_cases_14': {'type': 'float'},
                'new_cases_by_100k': {'type': 'float'},
                'total_cases_by_100k': {'type': 'float'},
                'active_cases_7_by_100k': {'type': 'float'},
                'active_cases_14_by_100k': {'type': 'float'},
                'new_cases_mean_7': {'type': 'float'},
                'new_cases_mean_14': {'type': 'float'},
                'max_new_cases': {'type': 'float'},
                'max_new_cases_date': {'type': 'string'},
                'd': {'type': 'dict'},
                'c': {'type': 'dict'},
                'area': {'type': 'float'},
                'centroid': {'type': 'list'},
                'was_missing': {'type': 'bool'},
                'viajes': {'type': 'float'},
                'personas': {'type': 'float'},
            },
        },
        'layers.overlaps': {
            'description': descriptions.layers_overlaps,
            # 'allow_unknown': True,
            'item_title': 'layers.overlaps',
            'schema': {
                'l': {
                    'type': 'dict',
                    'schema': {                
                        'id': {'type': 'string'},
                        'layer': {'type': 'string'},
                        'ratio': {'type': 'float'}
                    }
                },
                'm': {
                    'type': 'dict',
                    'schema': {                
                        'id': {'type': 'string'},
                        'layer': {'type': 'string'},
                        'ratio': {'type': 'float'}
                    }
                }
            },
        },
        'layers.overlaps_population': {
            'description': descriptions.layers_overlaps_population,
            # 'allow_unknown': True,
            'item_title': 'layers.overlaps_population',
            'schema': {
                'l': {
                    'type': 'dict',
                    'schema': {                
                        'id': {'type': 'string'},
                        'layer': {'type': 'string'},
                        'ratio': {'type': 'float'}
                    }
                },
                'm': {
                    'type': 'dict',
                    'schema': {                
                        'id': {'type': 'string'},
                        'layer': {'type': 'string'},
                        'ratio': {'type': 'float'}
                    }
                }
            },
        },
        'mitma_mov.movements_raw': {
            'description': descriptions.mitma_mov_movements_raw,
            'allow_unknown': True,
            'item_title': 'mitma_mov.movements_raw',
            'schema': {
                'origen': {'type': 'string'},
                'destino': {'type': 'string'},
                'role_origen': {'type': 'string'},
                'role_destino': {'type': 'string'},
                'viajes': {'type': 'float'},
                'viajes_km': {'type': 'float'},
                'distancia_min': {'type': 'float'},
                'distancia_max': {'type': 'float'},
                'residencia': {'type': 'string'},
                'evstart': {'type': 'datetime'},
                'evend': {'type': 'datetime'},
                'evday': {'type': 'datetime'},
            },
        },
        'mitma_mov.zone_movements': {
            'description': descriptions.mitma_mov_zone_movements,
            'allow_unknown': True,
            'item_title': 'mitma_mov.zone_movements',
            'schema': {
                'id': {'type': 'string'},
                'layer': {'type': 'string'},
                'evstart': {'type': 'datetime'},
                'evend': {'type': 'datetime'},
                'viajes': {'type': 'int'},
                'personas': {'type': 'float'},
            },
        },
        'mitma_mov.muni2trans': {
            'description': descriptions.empty,
            'allow_unknown': False,
            'item_title': 'mitma_mov.muni2trans',
            'schema': {
                'cudis': {'type': 'string'},
                'cumun': {'type': 'string'},
                'cpro': {'type': 'string'},
                'id_grupo_t': {'type': 'string'},
                'id_grupo_m': {'type': 'string'},
                'is_cudis': {'type': 'bool'},
            },
        },
        'mitma_mov.daily_mobility_matrix': {
            'description': descriptions.daily_mobility_matrix,
            # 'authentication': MitmaTokenAuth,
            'allow_unknown': True,
            'item_title': 'mitma_mov.daily_mobility_matrix',
            'schema': {
                'source': {'type': 'string'},
                'target': {'type': 'string'},
                'source_layer': {'type': 'string'},
                'target_layer': {'type': 'string'},
                'date': {'type': 'string'},
                'trips': {'type': 'float'},
                'updated_at': {'type': 'datetime'},
            },
        },
        'cnig.condominios': {
            'description': 'The content of this collection is obtained from the tables in a Microsoft Access database obtained from CNIG. No post processing is applied.',
            'allow_unknown': True,
            'item_title': 'cnig.condominios',
            'schema': {
                'idcon': {'type': 'int'},
                'cod_geografico': {'type': 'string'},
                'nombre_actual': {'type': 'string'},
                'provincia': {'type': 'string'},
                'municipios': {'type': 'string'},
                'idines': {'type': 'string'},
                'superficie': {'type': 'float'},
                'perimetro': {'type': 'float'},
                'longitud_etrs89': {'type': 'float'},
                'latitud_etrs89': {'type': 'float'},
                'origencoor': {'type': 'string'},
                'altitud': {'type': 'float'},
                'origenaltitud': {'type': 'string'},
            },
        },
        'cnig.eatims': {
            'description': 'The content of this collection is obtained from the tables in a Microsoft Access database obtained from CNIG. No post processing is applied.',
            'allow_unknown': True,
            'item_title': 'cnig.eatims',
            'schema': {
                'codinscrip': {'type': 'string'},
                'codprov': {'type': 'string'},
                'nombreprov': {'type': 'string'},
                'codauton': {'type': 'string'},
                'nombreauton': {'type': 'string'},
                'denominacion': {'type': 'string'},
                'codine': {'type': 'string'},
                'inemunicipio': {'type': 'string'},
                'longitud_etrs89': {'type': 'float'},
                'latitud_etrs89': {'type': 'float'},
                'origen_coor': {'type': 'string'},
                'altitud': {'type': 'float'},
                'origen_altitud': {'type': 'string'},
            },
        },
        'cnig.entidades': {
            'description': 'The content of this collection is obtained from the tables in a Microsoft Access database obtained from CNIG. No post processing is applied.',
            'allow_unknown': True,
            'item_title': 'cnig.entidades',
            'schema': {
                'codigoine': {'type': 'string'},
                'nombre': {'type': 'string'},
                'cod_prov': {'type': 'string'},
                'provincia': {'type': 'string'},
                'tipo': {'type': 'string'},
                'poblacion': {'type': 'string'},
                'inemuni': {'type': 'string'},
                'hoja_mtn25': {'type': 'string'},
                'longitud_etrs89': {'type': 'float'},
                'latitud_etrs89': {'type': 'float'},
                'origencoor': {'type': 'string'},
                'altitud': {'type': 'float'},
                'origenaltitud': {'type': 'string'},
                'suprimidaencarto': {'type': 'string'},
                'discrepanteine': {'type': 'string'},
            },
        },
        'cnig.municipios': {
            'description': 'The content of this collection is obtained from the tables in a Microsoft Access database obtained from CNIG. No post processing is applied.',
            'allow_unknown': True,
            'item_title': 'cnig.municipios',
            'schema': {
                'cod_ine': {'type': 'string'},
                'id_rel': {'type': 'string'},
                'cod_geo': {'type': 'string'},
                'cod_prov': {'type': 'string'},
                'provincia': {'type': 'string'},
                'nombre_actual': {'type': 'string'},
                'poblacion_muni': {'type': 'int'},
                'superficie': {'type': 'float'},
                'perimetro': {'type': 'float'},
                'cod_ine_capital': {'type': 'string'},
                'capital': {'type': 'string'},
                'poblacion_capital': {'type': 'string'},
                'hoja_mtn25_etrs89': {'type': 'string'},
                'longitud_etrs89': {'type': 'float'},
                'latitud_etrs89': {'type': 'float'},
                'origencoor': {'type': 'string'},
                'altitud': {'type': 'float'},
                'origenaltitud': {'type': 'string'},
            },
        },
        'cnig.provincias': {
            'description': 'The content of this collection is obtained from the tables in a Microsoft Access database obtained from CNIG. No post processing is applied.',
            'allow_unknown': True,
            'item_title': 'cnig.provincias',
            'schema': {
                'cod_prov': {'type': 'string'},
                'provincia': {'type': 'string'},
                'cod_ca': {'type': 'string'},
                'comunidad_autonoma': {'type': 'string'},
                'capital': {'type': 'string'},
            },
        },
        'ine.ccaa': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine.ccaa',
            'schema': {
                'cca': {'type': 'string'},
                'nombre_ca': {'type': 'string'},
            },
        },
        'ine.distritos': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine.distritos',
            'schema': {
                'cca': {'type': 'string'},
                'cpro': {'type': 'string'},
                'cudis': {'type': 'string'},
                'cumun': {'type': 'string'},
            },
        },
        'ine.municipios': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine.municipios',
            'schema': {
                'cca': {'type': 'string'},
                'cpro': {'type': 'string'},
                'cumun': {'type': 'string'},
                'nombre_municipio': {'type': 'string'},
            },
        },
        'ine.provincias': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine.provincias',
            'schema': {
                'cca': {'type': 'string'},
                'cpro': {'type': 'string'},
                'nombre_provincia': {'type': 'string'},
            },
        },
        'ine.sections': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine.sections',
            'schema': {
                'cusec': {'type': 'string'},
                'cumun': {'type': 'string'},
                'csec': {'type': 'string'},
                'cdis': {'type': 'string'},
                'cmun': {'type': 'string'},
                'cpro': {'type': 'string'},
                'cca': {'type': 'string'},
                'cudis': {'type': 'string'},
                'clau2': {'type': 'string'},
                'npro': {'type': 'string'},
                'nca': {'type': 'string'},
                'shape_leng': {'type': 'float'},
                'shape_area': {'type': 'float'},
                'superf_m2': {'type': 'int'},
                'nmun': {'type': 'string'},
            },
        },
        'ine_mov.area_geo': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.area_geo',
            'schema': {
                'cod_literal_scd': {'type': 'string'},
                'cod_provincia': {'type': 'string'},
                'cumun': {'type': 'string'},
                'id_area_geo': {'type': 'string'},
                'id_completo_grupo': {'type': 'string'},
                'id_grupo': {'type': 'string'},
                'pob_area_geo': {'type': 'int'},
                'tipo_area_geo': {'type': 'string'},
            },
        },
        'ine_mov.areas_diarias': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.areas_diarias',
            'schema': {
                'evstart': {'type': 'datetime'},
                'evend': {'type': 'datetime'},
                'evday': {'type': 'datetime'},
                'id_grupo': {'type': 'string'},
                'n_destino': {'type': 'int'},
                'pob_sale': {'type': 'int'},
                'pob_casa': {'type': 'int'},
                'p_pob_sale': {'type': 'float'},
                'p_pob_casa': {'type': 'float'},
                'n_origen': {'type': 'int'},
                'pob_llega': {'type': 'int'},
                'p_pob_llega': {'type': 'float'},
            },
        },
        'ine_mov.areas_movilidad': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.areas_movilidad',
            'schema': {
                'cod_provincia': {'type': 'string'},
                'id_grupo': {'type': 'string'},
            },
        },
        'ine_mov.grupo': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.grupo',
            'schema': {
                'id_grupo': {'type': 'string'},
                'pob_grupo': {'type': 'int'},
            },
        },
        'ine_mov.movements': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.movements',
            'schema': {
                'origen': {'type': 'string'},
                'destino': {'type': 'string'},
                'flujo': {'type': 'float'},
                'evstart': {'type': 'datetime'},
                'evend': {'type': 'datetime'},
            },
        },
        'ine_mov.municipio': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.municipio',
            'schema': {
                'cod_provincia': {'type': 'string'},
                'cumun': {'type': 'string'},
                'nombre_municipio': {'type': 'string'},
            },
        },
        'ine_mov.nacional_moviles': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.nacional_moviles',
            'schema': {
                'cumun': {'type': 'string'},
                'cod_provincia': {'type': 'string'},
                'nombre_provincia': {'type': 'string'},
                'nombre_municipio': {'type': 'string'},
                'tipo_area_geo': {'type': 'string'},
                'id_area_geo': {'type': 'string'},
                'pob_area_geo': {'type': 'int'},
                'cod_literal_scd': {'type': 'string'},
                'id_grupo': {'type': 'string'},
                'pob_grupo': {'type': 'int'},
                'id_completo_grupo': {'type': 'string'},
            },
        },
        'ine_mov.provincia': {
            'description': 'Contains information relative to the mobility dataset generated by the study performed by INE',
            'allow_unknown': True,
            'item_title': 'ine_mov.provincia',
            'schema': {
                'cod_provincia': {'type': 'string'},
                'nombre_provincia': {'type': 'string'},
            },
        },
        'layers.predictions': {
            'description': 'Contains predictions for several layers/values. Example: Covid19 Incidence predictions for layer `reg_san_obs_09` (Regiones Sanitarias de Catalunya). Predictions are usually provided for a window of several days in advance. The field `date_simulation` contains the last date used for the simulation: for example, the model may use incidence data up to 1/Ago, and predict 21 days from there on. Therefore, for each day we may have several predictions: the one generated using data up to the previous day, the one using data up to two days before, and so on.',
            'allow_unknown': True,
            'item_title': 'layers.predictions',
            'schema': {
                'id': {'type': 'string'},
                'layer': {'type': 'string'},
                'dataset': {'type': 'string'},
                'date': {'type': 'string'},
                'date_simulation': {'type': 'string'},
                'date_fetched': {'type': 'string'},
            },
        },
        'test': {
            'source': 'test',
            'allow_unknown': True,
            'item_title': 'test',
            'authentication': MitmaBasicAuth,
        },
        # 'test': {
        #     'source': 'test',
        #     'allow_unknown': True,
        #     'item_title': 'test',
        #     'authentication': MitmaTokenAuth,
        # },
        'provenance': {
            'description': descriptions.provenance,
            'allow_unknown': True,
            'item_title': 'provenance',
            'schema': {
                'storedAt': {'type': 'datetime'},
                'lastStoredAt': {'type': 'datetime'},
                'storedIn': {'type': 'string'},
                'storedBy': {'type': 'string'},
                'fetched': {
                    'type': 'list',
                    'schema': {
                        'from': {'type': 'string'},
                        'at': {'type': 'datetime'},
                        'by': {'type': 'string'},
                        'originalChecksum': {'type': 'string'},
                    }
                },
                'processedFrom': {'type': 'list'},
                'keywords': {'type': 'string'},
                'numEntries': {'type': 'int'},
                'status': {'type': 'string'},
                'error': {'type': 'string'},
            },
        },
    },
}
